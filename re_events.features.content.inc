<?php
/**
 * @file
 * re_events.features.content.inc
 */

/**
 * Implements hook_content_defaults().
 */
function re_events_content_defaults() {
  $content = array();

  $content['Event_1'] = (object) array(
    'exported_path' => 'events/family-fishing-day',
    'title' => 'Family Fishing Day',
    'status' => '1',
    'promote' => '0',
    'sticky' => '0',
    'type' => 'event',
    'language' => 'und',
    'created' => '1330124532',
    'comment' => '1',
    'translate' => '0',
    'machine_name' => 'Event_1',
    'body' => array(),
    'field_event_loc_geocode' => array(
      'und' => array(
        0 => array(
          'wkt' => 'POINT (-123.0683701 49.3218642)',
          'geo_type' => 'point',
          'lat' => '49.3219',
          'lon' => '-123.068',
          'left' => '-123.068',
          'top' => '49.3219',
          'right' => '-123.068',
          'bottom' => '49.3219',
          'srid' => NULL,
          'accuracy' => NULL,
          'source' => NULL,
        ),
      ),
    ),
    'field_event_location' => array(
      'und' => array(
        0 => array(
          'country' => 'CA',
          'administrative_area' => 'BC',
          'sub_administrative_area' => NULL,
          'locality' => 'North Vancouver',
          'dependent_locality' => NULL,
          'postal_code' => '',
          'thoroughfare' => '',
          'premise' => 'East 15th Street',
          'sub_premise' => NULL,
          'organisation_name' => '',
          'name_line' => NULL,
          'first_name' => NULL,
          'last_name' => NULL,
          'data' => NULL,
        ),
      ),
    ),
    'field_event_start_date' => array(
      'und' => array(
        0 => array(
          'value' => '2012-11-01 11:00:00',
          'value2' => '2012-11-01 11:00:00',
          'rrule' => NULL,
          'timezone' => 'America/Los_Angeles',
          'timezone_db' => 'America/Los_Angeles',
          'date_type' => 'datetime',
        ),
      ),
    ),
  );

  $content['Event_2'] = (object) array(
    'exported_path' => 'events/spinning-spectacular',
    'title' => 'Spinning Spectacular',
    'status' => '1',
    'promote' => '0',
    'sticky' => '0',
    'type' => 'event',
    'language' => 'und',
    'created' => '1330124469',
    'comment' => '1',
    'translate' => '0',
    'machine_name' => 'Event_2',
    'body' => array(),
    'field_event_loc_geocode' => array(
      'und' => array(
        0 => array(
          'wkt' => 'POINT (-123.1081775 49.2811834)',
          'geo_type' => 'point',
          'lat' => '49.2812',
          'lon' => '-123.108',
          'left' => '-123.108',
          'top' => '49.2812',
          'right' => '-123.108',
          'bottom' => '49.2812',
          'srid' => NULL,
          'accuracy' => NULL,
          'source' => NULL,
        ),
      ),
    ),
    'field_event_location' => array(
      'und' => array(
        0 => array(
          'country' => 'CA',
          'administrative_area' => 'BC',
          'sub_administrative_area' => NULL,
          'locality' => 'North Vancouver',
          'dependent_locality' => NULL,
          'postal_code' => '',
          'thoroughfare' => '',
          'premise' => '119 West Pender Street',
          'sub_premise' => NULL,
          'organisation_name' => '',
          'name_line' => NULL,
          'first_name' => NULL,
          'last_name' => NULL,
          'data' => NULL,
        ),
      ),
    ),
    'field_event_start_date' => array(
      'und' => array(
        0 => array(
          'value' => '2012-12-01 12:00:00',
          'value2' => '2012-12-01 12:00:00',
          'rrule' => NULL,
          'timezone' => 'America/Los_Angeles',
          'timezone_db' => 'America/Los_Angeles',
          'date_type' => 'datetime',
        ),
      ),
    ),
  );

  $content['Event_3'] = (object) array(
    'exported_path' => 'events/art-show',
    'title' => 'Art Show',
    'status' => '1',
    'promote' => '0',
    'sticky' => '0',
    'type' => 'event',
    'language' => 'und',
    'created' => '1330124438',
    'comment' => '1',
    'translate' => '0',
    'machine_name' => 'Event_3',
    'body' => array(),
    'field_event_loc_geocode' => array(
      'und' => array(
        0 => array(
          'wkt' => 'POINT (-122.3079505 47.6549716)',
          'geo_type' => 'point',
          'lat' => '47.655',
          'lon' => '-122.308',
          'left' => '-122.308',
          'top' => '47.655',
          'right' => '-122.308',
          'bottom' => '47.655',
          'srid' => NULL,
          'accuracy' => NULL,
          'source' => NULL,
        ),
      ),
    ),
    'field_event_location' => array(
      'und' => array(
        0 => array(
          'country' => 'US',
          'administrative_area' => 'WA',
          'sub_administrative_area' => NULL,
          'locality' => 'Seattle',
          'dependent_locality' => NULL,
          'postal_code' => '',
          'thoroughfare' => 'Mary Gates Hall',
          'premise' => '',
          'sub_premise' => NULL,
          'organisation_name' => 'University of Washington',
          'name_line' => NULL,
          'first_name' => NULL,
          'last_name' => NULL,
          'data' => NULL,
        ),
      ),
    ),
    'field_event_start_date' => array(
      'und' => array(
        0 => array(
          'value' => '2011-01-01 12:00:00',
          'value2' => '2011-01-01 12:00:00',
          'rrule' => NULL,
          'timezone' => 'America/Los_Angeles',
          'timezone_db' => 'America/Los_Angeles',
          'date_type' => 'datetime',
        ),
      ),
    ),
  );

return $content;
}
