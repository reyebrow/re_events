<?php
/**
 * @file
 * re_events.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function re_events_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:events
  $menu_links['main-menu:events'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'events',
    'router_path' => 'events',
    'link_title' => 'Events',
    'options' => array(
      'attributes' => array(
        'title' => 'Upcoming Events',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '1',
    'expanded' => '0',
    'weight' => '20',
  );
  // Exported menu link: main-menu:events/calendar
  $menu_links['main-menu:events/calendar'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'events/calendar',
    'router_path' => 'events',
    'link_title' => 'Full Calendar',
    'options' => array(
      'attributes' => array(
        'title' => 'Full Calendar of Events',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'events',
  );
  // Exported menu link: main-menu:events/past
  $menu_links['main-menu:events/past'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'events/past',
    'router_path' => 'events',
    'link_title' => 'Past Events',
    'options' => array(
      'attributes' => array(
        'title' => 'Past Events',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'events',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Events');
  t('Full Calendar');
  t('Past Events');

  return $menu_links;
}
